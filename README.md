# Exine Unpakcer

## 언팩 준비
1. 빌드
2. Exe 파일과 같은곳에 Exine 0.71 Resources 폴더를 옮김
3. 인수를 사용하여 언팩

## 인수
### UnDat
- *.dat 파일안에 있는 파일들 추출
```PowerShell
.\ExineUnpacker.exe undat --target "../../../../Exine 0.71 Resources" --output "UnDatFiles"
```
- \-t \-\-target
    - 기본값 "target"
- \-o \-\-output
    - 기본값 "output"

### UnYpf
- 이미지 추출
- action (애니메이션) 파일 추출
```PowerShell
.\ExineUnpacker.exe unypf --target "UnDatFiles" --output "UnYpfFiles"
```
- \-t \-\-target
    - 기본값 "output"
- \-o \-\-output
    - 기본값 "outputYpf"

### UnDoi
- 캐릭터 의상의 각각의 프레임 이미지별 헤어, 의상, 무기, 방패 드로우 순서 추출
```PowerShell
.\ExineUnpacker.exe undoi --target "UnDatFiles" --output "UnDoiFiles"
```
- \-t \-\-target
    - 기본값 "output"
- \-o \-\-output
    - 기본값 "outputDoi"

### UnMap
- 타일, Static 오브젝트, Light 정보 추출
- Portal 정보 추출 (Tiled Json 에서만 추출되는 상황)
- Tiled 용 Json 도 추출
- Tiled 타일이미지 경로는 "..\outputTile\" 로 되어야 합니다.
```PowerShell
.\ExineUnpacker.exe unmap --target "UnDatFiles" --output "UnMapFiles"
```
- \-t \-\-target
    - 기본값 "output"
- \-o \-\-output
    - 기본값 "outputMap"

### UnEffect
- *.eff 파일 파일 분석중
```PowerShell
.\ExineUnpacker.exe uneffect --target2 "UnDatFiles" --output "PackEffectFiles"
```
- \-t \-\-target
    - 기본값 "output"
- \-o \-\-output
    - 기본값 "outputEffect"

### PackTile
- 타일 이미지 패킹
- Tiled 를 위함
```PowerShell
.\ExineUnpacker.exe packtile --target "UnYpfFiles" --output "PackTileFiles"
```
- \-t \-\-target
    - 기본값 "outputYpf"
- \-o \-\-output
    - 기본값 "outputTile"

### PackTile2
- 실험적인 타일 이미지 패킹
```PowerShell
.\ExineUnpacker.exe packtile2 --target2 "UnYpfFiles" --output "PackTile2Files"
```
- \-t \-\-target
    - 기본값 "outputYpf"
- \-o \-\-output
    - 기본값 "outputTile2"
