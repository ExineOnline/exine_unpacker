﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExineUnpacker
{
    class Doi
    {
        public static void UnDois(string targetDir, string outputDir)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            DirectoryInfo targetRoot = new DirectoryInfo(targetDir);
            DirectoryInfo[] subDirs = targetRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);

            string[] files = Directory.GetFiles(targetDir, $"*.doi", System.IO.SearchOption.AllDirectories);

            foreach (string file in files)
            {
                UnDoi(file, Path.Combine(outputDir, Path.GetDirectoryName(file.Substring(targetDir.Length + 1))));
            }
        }

        private static void UnDoi(string target, string output)
        {
            string outputDirectory = Path.Combine(output);
            DirectoryInfo directoryInfo = new DirectoryInfo(outputDirectory);

            if (directoryInfo.Exists == false)
            {
                directoryInfo.Create();
            }

            string outPath = Path.Combine(outputDirectory, Path.GetFileNameWithoutExtension(target)+"_doi.json");

            FileStream fileStream;
            if (File.Exists(outPath))
                fileStream = new FileStream(outPath, FileMode.Truncate);
            else
                fileStream = new FileStream(outPath, FileMode.OpenOrCreate);
            StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8);


            ByteReader br = new ByteReader(target);

            ushort unknown;
            ushort count;

            br.Read(out unknown);
            br.Read(out count);

            streamWriter.WriteLine("{");
            streamWriter.WriteLine("    \"dois\": [");
            for (int i = 0; i < count; ++i)
            {
                byte data;

                br.Read(out data);
                int body = (data >> 6) & 3;
                int hair = (data >> 4) & 3;
                int weapon = (data >> 2) & 3;
                int shield = (data >> 0) & 3;

                streamWriter.Write($"        {{\"body\":{body}, \"hair\":{hair}, \"weapon\":{weapon}, \"shield\":{shield}}}");
                if (i != count - 1)
                    streamWriter.WriteLine(",");
            }

            streamWriter.WriteLine();
            streamWriter.WriteLine("    ]");
            streamWriter.WriteLine("}");
            streamWriter.Close();
            streamWriter.Dispose();
            fileStream.Close();
            fileStream.Dispose();
        }
    }
}
