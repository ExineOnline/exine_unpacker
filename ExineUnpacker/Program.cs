﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ExineUnpacker
{
    class Program
    {
        static void Main(string[] args)
        {
            string func = args[0].ToLower();

            switch (func)
            {
                case "undat":
                    {
                        string target = "target";
                        string output = "output";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("undet options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : undet -t target -o output");
                                    return;

                            }
                        }

                        Dat.UnpackDats(target, output);
                    }
                    break;
                case "unypf":
                    {
                        string target = "output";
                        string output = "outputYpf";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("unypf options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : unypf -t target -o output");
                                    return;

                            }
                        }

                        Ypf.UnYpfs(target, output);
                    }
                    break;
                case "undoi":
                    {
                        string target = "output";
                        string output = "outputDoi";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("undoi options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : undoi -t target -o output");
                                    return;

                            }
                        }

                        Doi.UnDois(target, output);
                    }
                    break;
                case "unmap":
                    {
                        string target = "output";
                        string output = "outputMap";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("unmap options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : unmap -t target -o output");
                                    return;

                            }
                        }

                        Map.UnMaps(target, output);
                    }
                    break;
                case "packtile":
                    {
                        string target = "outputYpf";
                        string output = "outputTile";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("packtile options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : packtile -t target -o output");
                                    return;

                            }
                        }

                        Tile.PackTiles(target, output);
                    }
                    break;
                case "packtile2":
                    {
                        string target = "outputYpf";
                        string output = "outputTile2";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("packtile2 options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : packtile2 -t target -o output");
                                    return;

                            }
                        }

                        Tile.PackTiles2(target, output);
                    }
                    break;
                case "uneffect":
                    {
                        string target = "output";
                        string output = "outputEffect";

                        for (int i = 1; i < args.Length; ++i)
                        {
                            string option = args[i].ToLower();

                            switch (option)
                            {
                                case "-t":
                                case "--target":
                                    target = args[++i];
                                    break;
                                case "-o":
                                case "--output":
                                    output = args[++i];
                                    break;
                                default:
                                    Console.WriteLine();
                                    Console.WriteLine("uneffect options");
                                    Console.WriteLine(" -t\t\ttarget path");
                                    Console.WriteLine(" --target\ttarget path");
                                    Console.WriteLine(" -o\t\toutput path");
                                    Console.WriteLine(" --output\toutput path");
                                    Console.WriteLine();
                                    Console.WriteLine("example : uneffect -t target -o output");
                                    return;

                            }
                        }

                        Effect.UnEffects(target, output);
                    }
                    break;
                default:
                    Console.WriteLine("Not Found Func..");
                    Console.WriteLine();
                    Console.WriteLine("undat");
                    Console.WriteLine("unypf");
                    Console.WriteLine("undoi");
                    Console.WriteLine("unmap");
                    Console.WriteLine("packtile");
                    Console.WriteLine("packtile2");
                    Console.WriteLine("uneffect");
                    Console.WriteLine();
                    break;
            }
        }
    }
}
