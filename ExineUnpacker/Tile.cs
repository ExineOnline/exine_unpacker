﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace ExineUnpacker
{
    class Tile
    {
        static int maxWidthCount = 35;
        static int maxHeightCount = 70;
        static int tileWidth = 48;
        static int tileHeight = 24;


        public static void PackTiles(string targetDir, string outputDir)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            DirectoryInfo targetRoot = new DirectoryInfo(targetDir);
            DirectoryInfo[] subDirs = targetRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);


            string title = $"| {"fileName",-70} | {"width",-8} | {"height",-10} |";

            Console.WriteLine(title);
            Console.WriteLine("".PadLeft(title.Length, '-'));

            string[] directories = Directory.GetDirectories(targetDir, "TS_*_Tile", SearchOption.AllDirectories);

            foreach (string directory in directories)
            {
                PackTile(directory, outputDir);
            }
        }

        private static void PackTile(string target, string outputDir)
        {
            string fileName = Path.GetFileNameWithoutExtension(target);

            DirectoryInfo directoryInfo = new DirectoryInfo(outputDir);

            if (directoryInfo.Exists == false)
            {
                directoryInfo.Create();
            }

            Console.WriteLine(target);

            string[] files = Directory.GetFiles(target, $"TS_*_Tile_*_*_size_*_*_pivot_*_*.png", System.IO.SearchOption.AllDirectories);
            Array.Sort(files, 
                (string s1, string s2) =>
                {
                    var pattern = @"TS_\d+_Tile_(?<imageIndex>\d+)_(?<frameIndex>\d+)_";
                    var regex = new Regex(pattern);

                    var s1FileName = Path.GetFileNameWithoutExtension(s1);
                    var s2FileName = Path.GetFileNameWithoutExtension(s2);

                    int s1ImageIndex;
                    int s1FrameIndex;
                    int s2ImageIndex;
                    int s2FrameIndex;

                    try
                    {
                        var s1Groups = regex.Match(s1FileName).Groups;
                        if (s1Groups.Count == 1)
                        {
                            return 0;
                        }
                        int.TryParse(s1Groups["imageIndex"].Value, out s1ImageIndex);
                        int.TryParse(s1Groups["frameIndex"].Value, out s1FrameIndex);

                        var s2Groups = regex.Match(s2FileName).Groups;
                        if (s2Groups.Count == 1)
                        {
                            return 0;
                        }
                        int.TryParse(s2Groups["imageIndex"].Value, out s2ImageIndex);
                        int.TryParse(s2Groups["frameIndex"].Value, out s2FrameIndex);

                        int compare = s1ImageIndex - s2ImageIndex;

                        if (compare == 0)
                        {
                            compare = s1FrameIndex - s2FrameIndex;
                        }

                        return compare;
                    }
                    catch
                    {
                        return 0;
                    }
                });

            int widthCount = files.Length < maxWidthCount ? files.Length : maxWidthCount;
            int heightCount = (int)Math.Ceiling((float)files.Length / maxWidthCount);



            Bitmap bmpOut = new Bitmap(widthCount * tileWidth, heightCount * tileHeight, PixelFormat.Format32bppArgb);
            BitmapData bmpOutData = bmpOut.LockBits(new Rectangle(0, 0, bmpOut.Width, bmpOut.Height), ImageLockMode.WriteOnly, bmpOut.PixelFormat);
            int bmpOutRowSize = Math.Abs(bmpOutData.Stride);


            int outX = 0;
            int outY = 0;

            foreach (string file in files)
            {
                //Console.WriteLine(file);

                int width;
                int height;
                int pivotX;
                int pivotY;

                if (TryGetSpritePivot(file, out width, out height, out pivotX, out pivotY) == false)
                    continue;

                pivotX = pivotX * -1;
                pivotY = pivotY - height;

                Bitmap bmpTile = new Bitmap(file);

                BitmapData bmpTileData = bmpTile.LockBits(new Rectangle(0, 0, bmpTile.Width, bmpTile.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
                int bmpTileRowSize = Math.Abs(bmpTileData.Stride);
                byte[] bmpTileBytes = new byte[bmpTileRowSize * bmpTile.Height];
                Marshal.Copy(bmpTileData.Scan0, bmpTileBytes, 0, bmpTileRowSize * bmpTile.Height);
                bmpTile.UnlockBits(bmpTileData);

                for (int y = 0; y < bmpTile.Height; ++y)
                {
                    IntPtr bmpOutScan0 = IntPtr.Add(bmpOutData.Scan0, 
                        y * bmpOutRowSize + 
                        outX * tileWidth * 4 + 
                        outY * tileHeight * bmpOutRowSize + 
                        pivotX * 4 + 
                        pivotY * bmpOutRowSize);
                    Marshal.Copy(bmpTileBytes, y * bmpTileRowSize, bmpOutScan0, bmpTile.Width * 4);
                }

                if (++outX >= maxWidthCount)
                {
                    outX = 0;
                    ++outY;
                }
            }



            bmpOut.UnlockBits(bmpOutData);
            bmpOut.Save(Path.Combine(outputDir, fileName + $"_{widthCount * tileWidth}_{heightCount * tileHeight}.png"), ImageFormat.Png);
        }



        public static void PackTiles2(string targetDir, string outputDir)
        {
            tileWidth = 48;
            tileHeight = 24;
            maxWidthCount = 2048 / tileWidth;
            maxHeightCount = 2048 / tileHeight;

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            DirectoryInfo targetRoot = new DirectoryInfo(targetDir);
            DirectoryInfo[] subDirs = targetRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);


            string title = $"| {"fileName",-70} | {"width",-8} | {"height",-10} |";

            Console.WriteLine(title);
            Console.WriteLine("".PadLeft(title.Length, '-'));

            string[] directories = Directory.GetDirectories(targetDir, "TS_*_Tile", SearchOption.AllDirectories);

            foreach (string directory in directories)
            {
                PackTile2(directory, outputDir);
            }
        }

        private static void PackTile2(string target, string outputDir)
        {
            string fileName = Path.GetFileNameWithoutExtension(target);

            DirectoryInfo directoryInfo = new DirectoryInfo(outputDir);

            if (directoryInfo.Exists == false)
            {
                directoryInfo.Create();
            }

            Console.WriteLine(target);

            string[] files = Directory.GetFiles(target, $"TS_*_Tile_*_*_size_*_*_pivot_*_*.png", System.IO.SearchOption.AllDirectories);
            Array.Sort(files, 
                (string s1, string s2) =>
                {
                    var pattern = @"TS_\d+_Tile_(?<imageIndex>\d+)_(?<frameIndex>\d+)_";
                    var regex = new Regex(pattern);

                    var s1FileName = Path.GetFileNameWithoutExtension(s1);
                    var s2FileName = Path.GetFileNameWithoutExtension(s2);

                    int s1ImageIndex;
                    int s1FrameIndex;
                    int s2ImageIndex;
                    int s2FrameIndex;

                    try
                    {
                        var s1Groups = regex.Match(s1FileName).Groups;
                        if (s1Groups.Count == 1)
                        {
                            return 0;
                        }
                        int.TryParse(s1Groups["imageIndex"].Value, out s1ImageIndex);
                        int.TryParse(s1Groups["frameIndex"].Value, out s1FrameIndex);

                        var s2Groups = regex.Match(s2FileName).Groups;
                        if (s2Groups.Count == 1)
                        {
                            return 0;
                        }
                        int.TryParse(s2Groups["imageIndex"].Value, out s2ImageIndex);
                        int.TryParse(s2Groups["frameIndex"].Value, out s2FrameIndex);

                        int compare = s1ImageIndex - s2ImageIndex;

                        if (compare == 0)
                        {
                            compare = s1FrameIndex - s2FrameIndex;
                        }

                        return compare;
                    }
                    catch
                    {
                        return 0;
                    }
                });

            int widthCount = files.Length < maxWidthCount ? files.Length : maxWidthCount;
            int heightCount = (int)Math.Ceiling((float)files.Length / maxWidthCount);



            Bitmap bmpOut = new Bitmap(widthCount * tileWidth, heightCount * tileHeight, PixelFormat.Format32bppArgb);
            BitmapData bmpOutData = bmpOut.LockBits(new Rectangle(0, 0, bmpOut.Width, bmpOut.Height), ImageLockMode.WriteOnly, bmpOut.PixelFormat);
            int bmpOutRowSize = Math.Abs(bmpOutData.Stride);


            int outX = 0;
            int outY = 0;

            foreach (string file in files)
            {
                //Console.WriteLine(file);

                int width;
                int height;
                int pivotX;
                int pivotY;

                if (TryGetSpritePivot(file, out width, out height, out pivotX, out pivotY) == false)
                    continue;

                pivotX = pivotX * -1;
                pivotY = pivotY - height;

                Bitmap bmpTile = new Bitmap(file);

                BitmapData bmpTileData = bmpTile.LockBits(new Rectangle(0, 0, bmpTile.Width, bmpTile.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
                int bmpTileRowSize = Math.Abs(bmpTileData.Stride);
                byte[] bmpTileBytes = new byte[bmpTileRowSize * bmpTile.Height];
                Marshal.Copy(bmpTileData.Scan0, bmpTileBytes, 0, bmpTileRowSize * bmpTile.Height);
                bmpTile.UnlockBits(bmpTileData);

                for (int y = 0; y < bmpTile.Height; ++y)
                {
                    IntPtr bmpOutScan0 = IntPtr.Add(bmpOutData.Scan0, 
                        y * bmpOutRowSize + 
                        outX * tileWidth * 4 + 
                        outY * tileHeight * bmpOutRowSize + 
                        pivotX * 4 + 
                        pivotY * bmpOutRowSize);
                    Marshal.Copy(bmpTileBytes, y * bmpTileRowSize, bmpOutScan0, bmpTile.Width * 4);
                }

                if (++outX >= maxWidthCount)
                {
                    outX = 0;
                    ++outY;
                }
            }



            bmpOut.UnlockBits(bmpOutData);
            bmpOut.Save(Path.Combine(outputDir, fileName + $".png"), ImageFormat.Png);
        }



        static bool TryGetSpritePivot(string file, out int width, out int height, out int pivotX, out int pivotY)
        {
            width = 0;
            height = 0;
            pivotX = 0;
            pivotY = 0;

            var filename = Path.GetFileNameWithoutExtension(file);
            var pattern = @"size_(?<width>\d+)_(?<height>\d+)_pivot_(?<pivotX>(-\d+|\d+))_(?<pivotY>(-\d+|\d+))";
            var regex = new Regex(pattern);

            try
            {
                var groups = regex.Match(filename).Groups;
                if (groups.Count == 1)
                {
                    return false;
                }
                int.TryParse(groups["width"].Value, out width);
                int.TryParse(groups["height"].Value, out height);
                int.TryParse(groups["pivotX"].Value, out pivotX);
                int.TryParse(groups["pivotY"].Value, out pivotY);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
