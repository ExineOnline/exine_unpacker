﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExineUnpacker
{
    class Dat
    {
        public static void UnpackDats(string targetDir, string outputDir)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            DirectoryInfo targetRoot = new DirectoryInfo(targetDir);
            DirectoryInfo[] subDirs = targetRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);


            string title = $"| {"target",-70} | {"start",-8} | {"size",-10} | {"output",-70} |";

            Console.WriteLine(title);
            Console.WriteLine("".PadLeft(title.Length, '-'));


            string[] files = Directory.GetFiles(targetDir, $"*.dat", System.IO.SearchOption.AllDirectories);

            foreach (string file in files)
            {
                UnpackDat(file, Path.Combine(outputDir, Path.GetDirectoryName(file.Substring(targetDir.Length + 1))));
            }
        }

        public static void UnpackDat(string target, string output)
        {
            ByteReader br = new ByteReader(target);

            string outputDirectory = Path.Combine(output, Path.GetFileNameWithoutExtension(target));
            DirectoryInfo directoryInfo = new DirectoryInfo(outputDirectory);

            if (directoryInfo.Exists == false)
            {
                directoryInfo.Create();
            }

            int filesCount = BitConverter.ToInt32(br.buffer) - 1;
            for (int i = 4; i < filesCount * 36; i += 36)
            {
                if (br.buffer[4 + i] == 0x00)
                    break;

                string fileName = Encoding.GetEncoding("euc-kr").GetString(br.buffer, 4 + i, 32);

                int zeroIndex = fileName.IndexOf('\0');
                if (zeroIndex != -1)
                {
                    fileName = fileName.Substring(0, zeroIndex);
                }

                int startIndex = BitConverter.ToInt32(br.buffer, i);
                int endIndex;
                if (36 + i < br.buffer.Length)
                    endIndex = BitConverter.ToInt32(br.buffer, 36 + i);
                else
                    endIndex = startIndex;

                try
                {
                    string outPath = Path.Combine(outputDirectory, fileName);

                    FileStream fileStream;
                    if (File.Exists(outPath))
                        fileStream = new FileStream(outPath, FileMode.Truncate);
                    else
                        fileStream = new FileStream(outPath, FileMode.OpenOrCreate);
                    fileStream.Write(br.buffer, startIndex, endIndex - startIndex);
                    fileStream.Close();
                    fileStream.Dispose();

                    Console.WriteLine($"| {target.PadRight(target.Length + 70 - Encoding.GetEncoding("euc-kr").GetByteCount(target))} | {startIndex,8:X8} | {endIndex - startIndex,10:D} | {outPath.PadRight(outPath.Length + 70 - Encoding.GetEncoding("euc-kr").GetByteCount(outPath))} |");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }
    }
}
