﻿#define DETAIL_DEBUG

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ExineUnpacker
{
    class Effect
    {
        [Conditional("DETAIL_DEBUG")]
        public static void DWrite(string mes)
        {
            Console.Write(mes);
        }

        [Conditional("DETAIL_DEBUG")]
        public static void DWriteLine()
        {
            Console.WriteLine();
        }

        [Conditional("DETAIL_DEBUG")]
        public static void DWriteLine(string mes)
        {
            Console.WriteLine(mes);
        }

        public static void UnEffects(string targetDir, string outputDir)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            DirectoryInfo targetRoot = new DirectoryInfo(targetDir);
            DirectoryInfo[] subDirs = targetRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);

            string[] files = Directory.GetFiles(targetDir, $"*.eff", System.IO.SearchOption.AllDirectories);

            foreach (string file in files)
            {
                UnEffect(file, Path.Combine(outputDir, Path.GetDirectoryName(file.Substring(targetDir.Length + 1))));
            }

            string[] testArray = System.Linq.Enumerable.ToArray(test);
            Array.Sort(testArray, (string a, string b) => { 
                return a.Substring(16).CompareTo(b.Substring(16)); 
            });

            foreach (string testa in testArray)
            {
                DWriteLine(testa);
            }
        }


        static HashSet<string> test = new HashSet<string>();

        private static void UnEffect(string target, string output)
        {
            ByteReader br = new ByteReader(target);


            short _00_effVer;
            short _02_check;
            short _04_length;
            byte _06_check; // 아마도 플래그 또는 타입

            br.Read(out _00_effVer);
            br.Read(out _02_check);
            br.Read(out _04_length);
            br.Read(out _06_check);


            //if (_02_check == 0) // debug
            //{
            //    // _06_check == 0 = _02_check == 0 | 2 | 4 | 7 | 11 | 12 | 21 
            //    // _06_check == 2 = _02_check == 1 | 2 | 3 | 4 | 11 
            //    // _06_check == 7 = _02_check == 0 | 1

            //    return;
            //}

            //if ( false
            //    //|| _04_length == 1
            //    //|| _04_length == 2
            //    //|| _04_length == 3
            //    //|| _04_length == 4
            //    //|| _04_length == 5
            //    //|| _04_length == 6
            //    //|| _04_length == 7
            //    //|| _04_length == 8
            //    //|| _04_length == 9
            //    //|| _04_length == 10
            //    //|| _04_length == 11
            //    //|| _04_length == 12
            //    //|| _04_length == 13
            //    //|| _04_length == 14
            //    //|| _04_length == 15
            //    //|| _04_length == 17
            //    //|| _04_length == 18
            //    //|| _04_length == 22
            //    //|| _04_length == 27
            //    //|| _04_length == 29
            //    //|| _04_length == 31
            //    //|| _04_length == 37
            //    ) // debug
            //{
            //    // 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 17 | 18 | 22 | 27 | 29 | 31 | 37

            //    // _06_check == 0 = _04_length == 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 14 | 13 | 15 | 17 | 18 | 22 | 27 | 29 | 31
            //    // _06_check == 2 = _04_length == 2 | 4 | 5 | 7 | 13 | 14 | 17 | 37
            //    // _06_check == 7 = _04_length == 1 | 2 | 3 | 4
            //    return;
            //}

            //_06_check == 0 | 2 | 7
            //if (_06_check != 7) // debug
            //{
            //    return;
            //}

            if (_02_check != 1)
                return;

            DWrite($"{target.PadRight(target.Length + 70 - Encoding.GetEncoding("euc-kr").GetByteCount(target))} ");

            DWrite($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4}");

            if (_06_check == 0)
            {
                // langth:  1972      8    0    2    0    0
                // langth:  1975      8    0    2    0    0
                // langth:  1980      8    0    3    0    0
                // langth:  1978      8    0    3    0    0
                // langth:  1983      8    0    3    0    0
                // langth:  1977      8    0    3    0    0
                // langth:  1986      8    0    4    0    0
                // langth:  1988      8    0    4    0    1
                // langth:  3941      8    0    4    0    1
                // langth:  3949      8    0    5    0    0
                // langth:  3946      8    0    5    0    0
                // langth:  3949      8    0    5    0    1
                // langth:  3954      8    0    5    0    1
                // langth:  3951      8    0    5    0    1
                // langth:  3955      8    0    6    0    1
                // langth:  3959      8    0    6    0    1
                // langth:  3967      8    0    7    0    1
                // langth:  5923      8    0    7    0    1
                // langth:  5920      8    0    7    0    1
                // langth:  3964      8    0    7    0    1
                // langth:  5924      8    0    8    0    0
                // langth:  5924      8    0    8    0    1
                // langth:  5928      8    0    8    0    1
                // langth:  5931      8    0    9    0    0
                // langth:  5938      8    0    9    0    1
                // langth:  7892      8    0    9    0    1
                // langth:  5951      8    0   11    0    0
                // langth:  9862      8    0   11    0    3
                // langth:  9876      8    0   13    0    0
                // langth:  7935      8    0   13    0    1
                // langth:  9892      8    0   14    0    1
                // langth:  5978      8    0   14    0    1
                // langth:  9896      8    0   15    0    2
                // langth:  4213      8    0   15    0    4
                // langth: 11875      8    0   18    0    1
                // langth: 10133      8    0   22    0    5
                // langth: 21753      8    0   29    0    2
                // langth: 18250      8    0   31    0    3

                // langth:  4531      8    1   14    0    0

                // langth:  1990      8    2    4    0    0
                // langth:  3944      8    2    4    0    0
                // langth:  3949      8    2    5    0    0
                // langth:  3952      8    2    5    0    0
                // langth:  3951      8    2    5    0    0
                // langth: 15768      8    2   17    0    0

                // langth:  3959      8    3    6    0    0
                // langth:  3965      8    3    7    0    1
                // langth:  5938      8    3    9    0    1

                // langth:  1996      8    4    5    0    0
                // langth:  3959      8    4    6    0    0
                // langth:  3965      8    4    6    0    0
                // langth:  7905      8    4   10    0    1
                // langth:  7978      8    4   12    0    1
                // langth:  7911      8    4   12    0    1

                // langth:  3965      8    5    7    0    0
                // langth:  5938      8    5    9    0    1

                // langth:  9947      8    6   14    0    1

                // langth:  3992      8    7    9    0    1
                // langth:  7923      8    7   12    0    2

                // langth:  5954      8   10   11    0    1

                // langth:  7933      8   11   14    0    1

                // langth:  9890      8   12   13    0    4

                // langth: 23654      8   21   27    0    1


                byte _0_07_unk_check;
                byte _0_08_unk_check;

                br.Read(out _0_07_unk_check);
                br.Read(out _0_08_unk_check);


                short _63_imageIndex;
                
                br.index = 63;
                if (_0_07_unk_check == 1)
                    br.index += 3;

                br.Read(out _63_imageIndex);
                DWrite($" {_63_imageIndex,4}");
                DWrite($" {_0_07_unk_check,4} {_0_08_unk_check,4}");

                if (test.Contains($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4} {_0_07_unk_check,4}") == false)
                {
                    test.Add($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4} {_0_07_unk_check,4}");
                }

                // TODO
            }

            if (_06_check == 2)
            {
                // langth:  1972      8    1    2    2
                // langth:  3951      8    1    5    2
                // langth:  5920      8    1    7    2
                // langth: 31553      8    1   37    2

                // langth:  1986      8    2    4    2
                // langth:  3965      8    2    7    2

                // langth:  1986      8    3    4    2
                // langth: 11864      8    3   14    2
                // langth: 11885      8    3   17    2

                // langth:  1996      8    4    5    2

                // langth:  7923      8   11   13    2

                short _2_07_imageIndex;
                br.Read(out _2_07_imageIndex);
                DWrite($" {_2_07_imageIndex, 4}");

                byte _2_09;
                br.Read(out _2_09);
                DWrite($" {_2_09,4}");

                byte _2_10;
                br.Read(out _2_10);
                DWrite($" {_2_10,4}");

                byte _2_11;
                br.Read(out _2_11);
                DWrite($" {_2_11,4}");


                if (test.Contains($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4} {_2_09,4} {_2_10,4} {_2_11,4}") == false)
                {
                    test.Add($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4} {_2_09,4} {_2_10,4} {_2_11,4}");
                }

                // TODO
            }


            if (_06_check == 7) // TODO 알수없음 의미찾기. 아마도 조명 정보
            {
                // langth:    11      8    0    1    7    0

                // langth:    24      8    0    2    7    1

                // langth:    18      8    1    2    7    0
                // langth:    21      8    1    2    7    0
                // langth:    31      8    1    3    7    0
                // langth:    38      8    1    4    7    0

                // langth:    51      8    1    4    7    1

                byte _7_07_check;
                br.Read(out _7_07_check);


                if (test.Contains($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4} {_7_07_check,4}") == false)
                {
                    test.Add($"langth:{br.buffer.Length,6}   {_00_effVer,4} {_02_check,4} {_04_length,4} {_06_check,4} {_7_07_check,4}");
                }

                DWrite($" {_7_07_check,4}");

                switch (_7_07_check)
                {
                    case 0:
                        {
                            byte _7_08;
                            short _7_09;
                            br.Read(out _7_08);
                            br.Read(out _7_09);

                            DWrite($" {_7_08,4}");
                            DWrite($" {_7_09,4}");
                        }
                        break;
                    case 1:
                        {

                        }
                        break;
                    default:
                        break;
                }
            }

            DWriteLine("");



            //short min = short.Parse(Path.GetFileNameWithoutExtension(target).Substring(6, 5));
            //min = (short)((min / 1000) * 1000);
            //short max = (short)(min + 1000);


            //for (int i = 0; br.index + i < br.buffer.Length - 1; ++i)
            //{
            //    short cur = BitConverter.ToInt16(br.buffer, i);

            //    if (min < cur && cur < max)
            //    {
            //        Console.WriteLine(cur);
            //    }
            //}


            ////if (br.buffer.Length > 63)
            ////{
            ////    short eff_id;
            ////    br.index = 63;
            ////    br.Read(out eff_id);

            ////    DWrite($"{eff_id,5}");
            ////}

            //if (hasUnkPadding == 1 && br.buffer.Length > 1000)
            //    DWrite("err");
            //if (hasUnkPadding == 0 && br.buffer.Length < 1000)
            //    DWrite("err");
            //if (hasUnkPadding > 1 && br.buffer.Length < 1000)
            //    DWrite("err");
            //if (hasUnkPadding > 1)
            //{
            //    DWrite("err");
            //}


            //string outputDirectory = Path.Combine(output, Path.GetFileNameWithoutExtension(target));
            //DirectoryInfo directoryInfo = new DirectoryInfo(outputDirectory);

            //if (directoryInfo.Exists == false)
            //{
            //    directoryInfo.Create();
            //}

            //string outPath = Path.Combine(outputDirectory, Path.GetFileNameWithoutExtension(target) + "_effect.json");
        }
    }
}
