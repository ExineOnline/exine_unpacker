﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExineUnpacker
{
    class Portals
    {
        public Dictionary<short, List<Portal>> portals = new Dictionary<short, List<Portal>>();

        public Portals(string path)
        {
            ByteReader br = new ByteReader(path);

            short unk;
            uint count;

            br.Read(out unk);
            br.Read(out count);

            for (int i = 0; i < count; ++i)
            {
                Portal portal = new Portal();
                br.Read(out portal.mapId);
                br.Read(out portal.x);
                br.Read(out portal.y);

                if (portals.ContainsKey(portal.mapId) == false)
                {
                    portals.Add(portal.mapId, new List<Portal>());
                }
                portals[portal.mapId].Add(portal);
            }
        }
    }

    class Portal
    {
        public short mapId;
        public short x;
        public short y;
    }
}
