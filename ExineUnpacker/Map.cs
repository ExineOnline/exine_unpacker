﻿#define DETAIL_DEBUG
#define SAVE_JSON
#define SAVE_TILED_JSON


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace ExineUnpacker
{
    class Map
    {
        [Conditional("DETAIL_DEBUG")]
        public static void DWrite(string mes)
        {
            Console.Write(mes);
        }

        [Conditional("DETAIL_DEBUG")]
        public static void DWriteLine(string mes)
        {
            Console.WriteLine(mes);
        }

        public static void UnMaps(string targetDir, string outputDir)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            DirectoryInfo targetRoot = new DirectoryInfo(targetDir);
            DirectoryInfo[] subDirs = targetRoot.GetDirectories("*.*", System.IO.SearchOption.AllDirectories);


            string title = $"| {"file path",-70} | {"width",-5} | {"height",-5} | world0 | world1 | world2 |";

            Console.WriteLine(title);
            Console.WriteLine("".PadLeft(title.Length, '-'));


            Portals portals = null;


            string[] portals_tbls = Directory.GetFiles(targetDir, $"portals.tbl", System.IO.SearchOption.AllDirectories);

            if (portals_tbls.Length > 0)
            {
                portals = new Portals(portals_tbls[0]);
            }



            string[] files = Directory.GetFiles(targetDir, $"*.map", System.IO.SearchOption.AllDirectories);

            foreach (string file in files)
            {
                UnMap(file, Path.Combine(outputDir, Path.GetDirectoryName(file.Substring(targetDir.Length + 1))), portals);
            }
        }

        public static void UnMap(string target, string output, Portals portals)
        {
#if SAVE_JSON
            string outputDirectory = Path.Combine(output, Path.GetFileNameWithoutExtension(target), "..\\");
            DirectoryInfo directoryInfo = new DirectoryInfo(outputDirectory);

            if (directoryInfo.Exists == false)
            {
                directoryInfo.Create();
            }

            string outPath = Path.Combine(outputDirectory, Path.GetFileNameWithoutExtension(target) + "_map.json");

            FileStream fileStream;
            if (File.Exists(outPath))
                fileStream = new FileStream(outPath, FileMode.Truncate);
            else
                fileStream = new FileStream(outPath, FileMode.OpenOrCreate);
            StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8);
#endif

#if SAVE_TILED_JSON
            string outputTieldDirectory = Path.Combine(output, Path.GetFileNameWithoutExtension(target), "..\\Tield");
            DirectoryInfo tieldDirectoryInfo = new DirectoryInfo(outputTieldDirectory);

            if (tieldDirectoryInfo.Exists == false)
            {
                tieldDirectoryInfo.Create();
            }

            string outTieldPath = Path.Combine(outputTieldDirectory, Path.GetFileNameWithoutExtension(target) + "_map.json");

            FileStream tieldFileStream;
            if (File.Exists(outTieldPath))
                tieldFileStream = new FileStream(outTieldPath, FileMode.Truncate);
            else
                tieldFileStream = new FileStream(outTieldPath, FileMode.OpenOrCreate);
            StreamWriter tieldStreamWriter = new StreamWriter(tieldFileStream, Encoding.UTF8);
#endif


            ByteReader br = new ByteReader(target);

            // Header
            ushort[] worldId = new ushort[4];
            byte width;
            byte height;

            br.Read(out worldId[0]);
            br.Read(out worldId[1]);
            br.Read(out worldId[2]);

            br.Read(out width);
            br.Read(out height);

            bool isElsaMap = false;

            if (width == 0 || height == 0) // detected Elsa map
            {
                isElsaMap = true;
            }

            if (isElsaMap)
            {
                br.index -= 2;
                br.Read(out worldId[3]);

                br.Read(out width);
                br.Read(out height);
            }

            Console.WriteLine($"| {target.PadRight(target.Length + 70 - Encoding.GetEncoding("euc-kr").GetByteCount(target))} | {width,5} | {height,5} | {worldId[0],6} | {worldId[1],6} | {worldId[2],6} |");
            DWriteLine($"{br.index:X8} : End Header");

#if SAVE_JSON
            streamWriter.WriteLine("{");
            streamWriter.WriteLine($"    \"worldId\": [{worldId[0]}, {worldId[1]}, {worldId[2]}],");

            streamWriter.WriteLine($"    \"width\": {width},");
            streamWriter.WriteLine($"    \"height\": {height},");


            streamWriter.WriteLine("    \"MapAnim\": [");
#endif

            // MapAnim
            for (int y = 0; y <= (height - 1) / 4; ++y)
            {
#if SAVE_JSON
                if (y != 0)
                    streamWriter.WriteLine(",");
#endif

                for (int x = 0; x <= (width - 1) / 2; ++x)
                {
                    byte unk0;
                    byte unk1;

                    br.Read(out unk0);
                    br.Read(out unk1);

#if SAVE_JSON
                    if (x != 0)
                        streamWriter.Write(", ");
                    else
                        streamWriter.Write("        ");
                    streamWriter.Write($"[{unk0}, {unk1}]");
#endif
                }
            }
            DWriteLine($"{br.index:X8} : End MapAnim");
#if SAVE_JSON
            streamWriter.WriteLine();
            streamWriter.WriteLine("    ],");


            streamWriter.WriteLine("    \"MapTile\": [");
#endif

#if SAVE_TILED_JSON
            int[] layer_0 = new int[height * width];
            int[] layer_1 = new int[height * width];
            int[] layer_2 = new int[height * width];
            int[] layer_flags = new int[height * width];

            int tileInfoIndex = 0;
#endif
#if SAVE_TILED_JSON || SAVE_JSON
            int[] layer_portals = new int[height * width];
#endif

            // MapTile
            for (int y = 0; y < height; ++y)
            {
#if SAVE_JSON
                if (y != 0)
                    streamWriter.WriteLine(",");
#endif

                for (int x = 0; x < width; ++x)
                {
                    ushort[] tileIndex = new ushort[4];
                    int flag;

                    br.Read(out tileIndex[0]);
                    br.Read(out tileIndex[1]);
                    br.Read(out tileIndex[2]);
                    br.Read(out flag);

#if SAVE_TILED_JSON
                    layer_0[tileInfoIndex] = tileIndex[0] == 0 ? 0 : tileIndex[0] + worldId[0] * 100000;
                    layer_1[tileInfoIndex] = tileIndex[1] == 0 ? 0 : tileIndex[1] + worldId[1] * 100000;
                    layer_2[tileInfoIndex] = tileIndex[2] == 0 ? 0 : tileIndex[2] + worldId[2] * 100000;

                    layer_flags[tileInfoIndex] = flag == 0 ? 0 : flag + 1000000;

                    tileInfoIndex++;
#endif

                    byte[] flagBytes = BitConverter.GetBytes(flag);

                    DWrite($"\u001b[48;2;{flagBytes[0]};{flagBytes[1]};{flagBytes[2]}m\u001b[38;2;{(int)(tileIndex[0] * 255.0 / short.MaxValue)};{(int)(tileIndex[1] * 255.0 / short.MaxValue)};{(int)(tileIndex[2] * 255.0 / short.MaxValue)}m■\u001b[0m");

#if SAVE_JSON
                    if (x != 0)
                        streamWriter.Write(", ");
                    else
                        streamWriter.Write("        ");

                    streamWriter.Write($"{{\"tileIndex\": [{tileIndex[0]},{tileIndex[1]},{tileIndex[2]}], \"flag\": {flag}}}");
#endif
                }
                DWriteLine("");
            }

            string mapFileName = Path.GetFileNameWithoutExtension(target);
            short mapId = Int16.Parse(mapFileName);

            if (portals.portals.ContainsKey(mapId))
            {
                List<Portal> portals_list = portals.portals[mapId];
                for (int i = 0; i < portals_list.Count; ++i)
                {
                    layer_portals[portals_list[i].x + portals_list[i].y * width] = 1000038;
                }
            }

            DWriteLine($"{br.index:X8} : End MapTile");



#if SAVE_TILED_JSON
            string json =
                $"{{" +
                $"\n    \"backgroundcolor\": \"#656667\"," +
                $"\n    \"height\": {height}," +
                $"\n    \"layers\": [" +
                $"\n        {{" +
                $"\n            \"name\":\"ground 0\"," +
                $"\n            \"width\": {width}," +
                $"\n            \"height\": {height}," +
                $"\n            \"x\": 0," +
                $"\n            \"y\": 0," +
                $"\n            \"data\": [{String.Join(", ", layer_0)}]," +
                $"\n            \"opacity\": 1," +
                $"\n            \"type\": \"tilelayer\"," +
                $"\n            \"visible\": true," +
                $"\n            \"properties\": []" +
                $"\n        }}," +
                $"\n        {{" +
                $"\n            \"name\":\"ground 1\"," +
                $"\n            \"width\": {width}," +
                $"\n            \"height\": {height}," +
                $"\n            \"x\": 0," +
                $"\n            \"y\": 0," +
                $"\n            \"data\": [{String.Join(", ", layer_1)}]," +
                $"\n            \"opacity\": 1," +
                $"\n            \"type\": \"tilelayer\"," +
                $"\n            \"visible\": true," +
                $"\n            \"properties\": []" +
                $"\n        }}," +
                $"\n        {{" +
                $"\n            \"name\": \"ground 2\"," +
                $"\n            \"width\": {width}," +
                $"\n            \"height\": {height}," +
                $"\n            \"x\": 0," +
                $"\n            \"y\": 0," +
                $"\n            \"data\": [{String.Join(", ", layer_2)}]," +
                $"\n            \"opacity\": 1," +
                $"\n            \"type\": \"tilelayer\"," +
                $"\n            \"visible\": true," +
                $"\n            \"properties\": []" +
                $"\n        }}," +
                $"\n        {{" +
                $"\n            \"name\": \"collider\"," +
                $"\n            \"width\": {width}," +
                $"\n            \"height\": {height}," +
                $"\n            \"x\": 0," +
                $"\n            \"y\": 0," +
                $"\n            \"data\": [{String.Join(", ", layer_flags)}]," +
                $"\n            \"opacity\": 1," +
                $"\n            \"type\": \"tilelayer\"," +
                $"\n            \"visible\": true," +
                $"\n            \"properties\": []" +
                $"\n        }}," +
                $"\n        {{" +
                $"\n            \"name\": \"portals\"," +
                $"\n            \"width\": {width}," +
                $"\n            \"height\": {height}," +
                $"\n            \"x\": 0," +
                $"\n            \"y\": 0," +
                $"\n            \"data\": [{String.Join(", ", layer_portals)}]," +
                $"\n            \"opacity\": 1," +
                $"\n            \"type\": \"tilelayer\"," +
                $"\n            \"visible\": true," +
                $"\n            \"properties\": []" +
                $"\n        }}" +
                $"\n    ]," +
                $"\n    \"nextobjectid\": 1," +
                $"\n    \"orientation\": \"orthogonal\"," +
                $"\n    \"properties\": []," +
                $"\n    \"renderorder\": \"right-down\"," +
                $"\n    \"tileheight\": 24," +
                $"\n    \"tilesets\": [";

            string[] tileAtlasPaths  =  Directory.GetFiles("outputTile", "TS_*.png");


            var pattern = @"TS_?0(?<worldId>\d+)_Tile_(?<width>\d+)_(?<height>\d+)";
            var regex = new Regex(pattern);

            for (int i = 0; i < tileAtlasPaths.Length; ++i)
            {
                string tileAtlasPath = Path.GetFileNameWithoutExtension(tileAtlasPaths[i]);
                try
                {
                    var groups = regex.Match(tileAtlasPath).Groups;
                    if (groups.Count == 1)
                    {
                        continue;
                    }

                    int tileAtlasWorldId;
                    int tileAtlasWidth;
                    int tileAtlasHeight;

                    int.TryParse(groups["worldId"].Value, out tileAtlasWorldId);
                    int.TryParse(groups["width"].Value, out tileAtlasWidth);
                    int.TryParse(groups["height"].Value, out tileAtlasHeight);


                    if (i != 0)
                        json += ",";

                    json +=
                        $"\n        {{" +
                        $"\n            \"tilecount\": {(tileAtlasWidth / 48) * (tileAtlasHeight / 24)}," +
                        $"\n            \"tilewidth\": 48," +
                        $"\n            \"tileheight\": 24," +
                        $"\n            \"columns\": {tileAtlasWidth / 48}," +
                        $"\n            \"firstgid\": {1 + tileAtlasWorldId * 100000}," +
                        $"\n            \"margin\": 0," +
                        $"\n            \"spacing\": 0," +
                        $"\n            \"image\": \"..\\/..\\/..\\/outputTile\\/{Path.GetFileName(tileAtlasPath)}\"," +
                        $"\n            \"imagewidth\": {tileAtlasWidth}," +
                        $"\n            \"imageheight\": {tileAtlasHeight}," +
                        $"\n            \"name\": \"TS_{worldId:D2}_Tile\"" +
                        $"\n        }}";
                }
                catch
                {
                    continue;
                }
            }

            json +=
                $"\n    ]," +
                $"\n    \"tilewidth\": 48," +
                $"\n    \"version\": 1," +
                $"\n    \"tiledversion\": \"1.0.3\"," +
                $"\n    \"width\": {width}" +
                $"\n}}";
            tieldStreamWriter.WriteLine(json);
#endif


#if SAVE_JSON
            streamWriter.WriteLine();
            streamWriter.WriteLine("    ],");


            streamWriter.WriteLine("    \"StaticObjectInfo\": [");
#endif
            // StaticObjectInfo
            {
                short count;
                br.Read(out count);
                short count1 = 0;
                if (count != 0)
                    br.Read(out count1);
                DWriteLine($"StaticObjectInfo count {count,-5} {count1, -5}, ");

                for (int i = 0; i < count; ++i)
                {
                    byte isAnim;            // true : TS_ _AnimStatic[Shadows] false : TS_ _Static[Shadows]
                    short objectIndex;       // image index
                    ushort x;
                    ushort y;

                    br.Read(out isAnim);
                    br.Read(out objectIndex);
                    br.Read(out x);
                    br.Read(out y);

                    int world;
                    if (isElsaMap)
                    {
                        world = worldId[3];
                    }
                    else
                    {
                        world = objectIndex / 1000;
                    }
                    int index = objectIndex % 1000;

                    DWrite($"isAnim {isAnim,-5}, ");
                    DWrite($"world {world,-5}, ");
                    DWrite($"index {index,-5}, ");
                    DWrite($"x {x}, ");
                    DWrite($"y {y}, ");
                    DWriteLine("");

#if SAVE_JSON
                    if (i != 0)
                    {
                        streamWriter.WriteLine(",");
                        streamWriter.Write("        ");
                    }
                    else
                        streamWriter.Write("        ");
                    streamWriter.Write($"{{\"isAnim\": {isAnim}, \"world\": {world}, \"index\": {index}, \"x\": {x}, \"y\": {y}}}");
#endif
                }
            }
            DWriteLine($"{br.index:X8} : End StaticObjectInfo");
#if SAVE_JSON
            streamWriter.WriteLine();
            streamWriter.WriteLine("    ],");


            streamWriter.WriteLine("    \"LightInfo\": [");
#endif
//            // LightInfo
//            {
//                int count;
//                br.Read(out count);
//                DWriteLine($"LightInfo count {count}, ");

//                for (int i = 0; i < count; ++i)
//                {
//                    ushort unk1;
//                    ushort x;
//                    ushort y;
//                    ushort unk2;

//                    br.Read(out unk1);
//                    br.Read(out x);
//                    br.Read(out y);
//                    br.Read(out unk2);

//                    DWrite($"unk1 {unk1,-5}, ");
//                    DWrite($"x {x,-5}, ");
//                    DWrite($"y {y,-5}, ");
//                    DWrite($"unk2 {unk2,-5}, ");
//                    DWriteLine("");
//#if SAVE_JSON
//                    if (i != 0)
//                    {
//                        streamWriter.WriteLine(",");
//                        streamWriter.Write("        ");
//                    }
//                    else
//                        streamWriter.Write("        ");
//                    streamWriter.Write($"{{\"unk1\": {unk1}, \"x\": {x}, \"y\": {y}, \"unk2\": {unk2}}}");
//#endif
//                }
//#if SAVE_JSON
//                streamWriter.WriteLine();
//#endif
//            }
//            DWriteLine($"{br.index:X8} : End LightInfo");
//#if SAVE_JSON
//            streamWriter.WriteLine("    ],");


//            streamWriter.WriteLine("    \"MultiStateObjectInfo\": [");
//#endif
//            // MultiStateObjectInfo
//            {
//                int count;
//                br.Read(out count);
//                DWriteLine($"MultiStateObjectInfo count {count}, ");

//                for (int i = 0; i < count; ++i)
//                {
//                    ushort unk1;
//                    ushort unk2;
//                    uint   unk3;
//                    byte   alpha;
//                    byte   blue;
//                    byte   green;
//                    byte   red;
//                    ushort unk4;
//                    ushort unk5;

//                    br.Read(out unk1);
//                    br.Read(out unk2);
//                    br.Read(out unk3);
//                    br.Read(out alpha);
//                    br.Read(out blue);
//                    br.Read(out green);
//                    br.Read(out red);
//                    br.Read(out unk4);
//                    br.Read(out unk5);

//                    DWrite($"unk1 {unk1,-5}, ");
//                    DWrite($"unk2 {unk2,-5}, ");
//                    DWrite($"unk3 {unk3,-5}, ");
//                    DWrite($"alpha {alpha,-5}, ");
//                    DWrite($"blue {blue,-5}, ");
//                    DWrite($"green {green,-5}, ");
//                    DWrite($"red {red,-5}, ");
//                    DWrite($"unk4 {unk4,-5}, ");
//                    DWrite($"unk5 {unk5,-5}, ");
//                    DWriteLine("");
//                }
//            }
//            DWriteLine($"{br.index:X8} : End MultiStateObjectInfo");
#if SAVE_JSON
            streamWriter.WriteLine("    ],");


            streamWriter.WriteLine("    \"Portal\": [");
            // Portal
            {
                if (portals.portals.ContainsKey(mapId))
                {
                    List<Portal> portals_list = portals.portals[mapId];

                    for (int i = 0; i < portals_list.Count; ++i)
                    {
                        DWrite($"mapId {portals_list[i].mapId,-5}, ");
                        DWrite($"x {portals_list[i].x,-5}, ");
                        DWrite($"y {portals_list[i].y,-5}, ");
                        DWriteLine("");
                        if (i != 0)
                        {
                            streamWriter.WriteLine(",");
                            streamWriter.Write("        ");
                        }
                        else
                            streamWriter.Write("        ");

                        streamWriter.Write($"{{\"mapId\": {portals_list[i].mapId}, \"x\": {portals_list[i].x}, \"y\": {portals_list[i].y}}}");
                    }
                    streamWriter.WriteLine();
                }
            }
            DWriteLine($"{br.index:X8} : End Portal");
            streamWriter.WriteLine("    ]");


            streamWriter.WriteLine("}");

            streamWriter.Close();
            streamWriter.Dispose();
            fileStream.Close();
            fileStream.Dispose();
#endif


#if SAVE_TILED_JSON
            tieldStreamWriter.Close();
            tieldStreamWriter.Dispose();
            tieldFileStream.Close();
            tieldFileStream.Dispose();
#endif
        }
    }
}
